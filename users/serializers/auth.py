from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.tokens import RefreshToken


class TokenObtainSerializer(TokenObtainPairSerializer):
    token_class = RefreshToken

    def to_representation(self, instance):
        data = super(TokenObtainSerializer, self).to_representation(instance)
        refresh = self.get_token(self.user)

        data["refresh"] = str(refresh)
        data["access"] = str(refresh.access_token)
        return data
