from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from users.views import TokenObtainView

urlpatterns = [
    path('login/', TokenObtainView.as_view(), name="login"),
    path("token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
]
