from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = []

    email = models.EmailField(unique=True, null=True)
    username = models.CharField(max_length=150, unique=True)

    def __str__(self):
        return self.username
