from django.urls import path

from metsenat.views import (
    SponsorCreateView,
    SponsorUpdateView,
    StudentView,
    AddSponsoredAmount,
    EditSponsoredAmount,
    SponsoredAmountsRetrieveDeleteView,
    StudentUpdateView,
    StudentSponsorsListView,
    DashboardView
)

urlpatterns = [
    path('dashboard/', DashboardView.as_view(), name="dashboard_statistics"),
    path('sponsors/', SponsorCreateView.as_view(), name="sponsor_create"),
    path('<int:pk>/sponsor/', SponsorUpdateView.as_view(), name="sponsor_update"),
    path('students/', StudentView.as_view(), name="students_list_create"),
    path('student/<int:student_id>/', StudentUpdateView.as_view(), name="students_update_retrieve"),
    path('student/<int:student_id>/sponsors/', StudentSponsorsListView.as_view(), name="student_sponsors"),
    path('student/<int:student_id>/add-sponsor/', AddSponsoredAmount.as_view(), name="add_sponsor_to_student"),
    path('sponsored-amounts/<int:sp_amount_id>/edit/', EditSponsoredAmount.as_view(), name="edit_sponsored_amount"),
    path(
        'sponsored-amounts/<int:sp_amount_id>/',
        SponsoredAmountsRetrieveDeleteView.as_view(),
        name="edit_sponsored_amount"
    ),
]
