import calendar
from datetime import datetime

import pytz
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.db.models import Sum, Count
from django.db.models.functions import ExtractMonth, ExtractYear
from django.utils import timezone

from metsenat.models import Student, SponsoredAmounts, Sponsor


def get_statistics():
    tz = settings.TIME_ZONE
    requested_amount = Student.objects.aggregate(
        requested_amount=Sum("contract_amount")
    ).get("requested_amount") or 0
    paid_amount = SponsoredAmounts.objects.aggregate(
        paid_amount=Sum("amount")
    ).get("amount") or 0

    now = timezone.now()
    now_last_date = datetime(
        year=now.year, month=now.month, day=calendar.monthrange(now.year, now.month)[1], tzinfo=pytz.timezone(tz)
    )

    start_time = timezone.now() - relativedelta(months=11)
    start_first_date = datetime(year=start_time.year, month=start_time.month, day=1, tzinfo=pytz.timezone(tz))

    students = Student.objects.filter(
        created_at__range=[start_first_date, now_last_date]
    ).annotate(month=ExtractMonth('created_at'), year=ExtractYear('created_at')).order_by('year', 'month').values(
        'month', 'year').annotate(total=Count('*')).values('month', 'year', 'total')
    sponsors = Sponsor.objects.filter(
        created_at__range=[start_first_date, now_last_date]
    ).annotate(month=ExtractMonth('created_at'), year=ExtractYear('created_at')).order_by('year', 'month').values(
        'month', 'year').annotate(total=Count('*')).values('month', 'year', 'total')

    return {'requested_amount': requested_amount, 'paid_amount': paid_amount,
            'due_amount': requested_amount - paid_amount, 'students': students, 'sponsors': sponsors}
