from enum import Enum
from django.utils.translation import gettext_lazy as _


class StudentTypes(Enum):
    BACHELOR = "BACHELOR"
    MASTER = "MASTER"
    PROFESSOR = "PROFESSOR"

    @classmethod
    def choices(cls):
        return (
            (cls.BACHELOR.value, _("Bachelor")),
            (cls.MASTER.value, _("Master")),
            (cls.PROFESSOR.value, _("Professor")),
        )


class PersonalTypes(Enum):
    PHYSICAL_PERSON = "PHYSICAL_PERSON"
    LEGAL_ENTITY = "LEGAL_ENTITY"

    @classmethod
    def choices(cls):
        return (
            (cls.PHYSICAL_PERSON.value, _("Physical person")),
            (cls.LEGAL_ENTITY.value, _("Legal entity"))
        )


class Statuses(Enum):
    NEW = "NEW"
    CONFIRMED = "CONFIRMED"
    IN_MODERATION = "IN_MODERATION"
    REJECTED = "REJECTED"

    @classmethod
    def choices(cls):
        return (
            (cls.NEW.value, _("New")),
            (cls.CONFIRMED.value, _("Confirmed")),
            (cls.IN_MODERATION.value, _("In moderation")),
            (cls.REJECTED.value, _("Rejected"))
        )
