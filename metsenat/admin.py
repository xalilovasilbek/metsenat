from django.contrib import admin

# Register your models here.
from metsenat.models import Student, Sponsor, SponsoredAmounts

admin.site.register(Student)
admin.site.register(Sponsor)
admin.site.register(SponsoredAmounts)
