from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from metsenat.models import Student
from django.utils.translation import gettext_lazy as _


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = "__all__"

    def validate(self, attrs):
        if not self.instance:
            return attrs
        allocated_amount = self.instance.allocated_amount
        if allocated_amount > attrs["contract_amount"]:
            raise ValidationError(_(f"For this student already allocated {allocated_amount}"))
        return attrs

    def to_representation(self, instance):
        data = super(StudentSerializer, self).to_representation(instance)
        data["allocated_amount"] = instance.allocated_amount
        return data
