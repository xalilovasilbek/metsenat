from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from metsenat.enums import PersonalTypes
from metsenat.models import Sponsor


class SponsorListCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sponsor
        fields = "__all__"
        extra_kwargs = {"status": {"read_only": True}}

    def validate(self, attrs):
        if attrs["personal_type"] == PersonalTypes.LEGAL_ENTITY.value and not attrs["organization"]:
            raise ValidationError(_("Organization must not be null!"))
        elif attrs["personal_type"] == PersonalTypes.PHYSICAL_PERSON.value and attrs["organization"]:
            attrs["organization"] = None
        return attrs

    def to_representation(self, instance):
        data = super(SponsorListCreateSerializer, self).to_representation(instance)
        data["spent_amount"] = instance.spent_amount
        return data


class SponsorUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sponsor
        fields = "__all__"

    def validate(self, attrs):
        if attrs["personal_type"] == PersonalTypes.LEGAL_ENTITY.value and not attrs["organization"]:
            raise ValidationError(_("Organization must not be null!"))
        elif attrs["personal_type"] == PersonalTypes.PHYSICAL_PERSON.value and attrs["organization"]:
            attrs["organization"] = None
        spent_amount = self.instance.spent_amount
        if spent_amount > attrs["budget"]:
            raise ValidationError(_(f"Sponsor already sponsored {spent_amount}"))
        return attrs
