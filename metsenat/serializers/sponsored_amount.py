from rest_framework import serializers

from metsenat.models import SponsoredAmounts
from metsenat.serializers import StudentSerializer, SponsorListCreateSerializer


class AddSponsorSerializer(serializers.ModelSerializer):
    class Meta:
        model = SponsoredAmounts
        fields = ["id", "student", "sponsor", "amount"]
        extra_kwargs = {"student": {"read_only": True}}

    def to_representation(self, instance):
        data = super(AddSponsorSerializer, self).to_representation(instance)
        data["student"] = StudentSerializer(instance.student).data
        data["sponsor"] = SponsorListCreateSerializer(instance.sponsor).data
        return data


class UpdateSponsoredAmountSerializer(serializers.ModelSerializer):
    class Meta:
        model = SponsoredAmounts
        fields = ["student", "sponsor", "amount"]
        extra_kwargs = {"student": {"read_only": True}, "sponsor": {"read_only": True}}

