from rest_framework.exceptions import ValidationError

from metsenat.enums import Statuses
from django.utils.translation import gettext_lazy as _

from metsenat.models import SponsoredAmounts


class ValidateSponsoredAmount:
    def __init__(self, sponsor, student, amount):
        self.sponsor = sponsor
        self.student = student
        self.amount = amount
        self.student_space_amount = self.student.space_amount
        self.sponsor_balance = self.sponsor.balance

    def validate(self):
        if self.amount <= 0:
            raise ValidationError(_(f"Amount must be positive!"))
        self.check_existing_sponsor()
        self.check_sponsor_possibility()
        self.check_student_possibility()

    def check_existing_sponsor(self):
        if SponsoredAmounts.objects.filter(sponsor=self.sponsor, student=self.student):
            raise ValidationError(_(f"{self.sponsor.fullname} already sponsored to {self.student.fullname}!"))

    def check_sponsor_possibility(self):
        if self.sponsor.status != Statuses.CONFIRMED.value:
            raise ValidationError(_("Status of sponsor must be confirmed!"))

        if self.sponsor_balance < self.amount:
            raise ValidationError(_(f"{self.sponsor.fullname} can sponsor maximum {self.sponsor_balance}"))

    def check_student_possibility(self):
        if self.amount > self.student_space_amount:
            raise ValidationError(_(f"{self.student.fullname} can receive maximum {self.student_space_amount}"))


class ValidateExistingSponsoredAmount(ValidateSponsoredAmount):
    def __init__(self, sponsor, student, amount, sponsored_amount):
        super(ValidateExistingSponsoredAmount, self).__init__(sponsor, student, amount)
        self.sponsor_balance = self.sponsor_balance + sponsored_amount.amount
        self.student_space_amount = self.student_space_amount + sponsored_amount.amount

    def check_existing_sponsor(self):
        pass
