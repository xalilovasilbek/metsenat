from django.shortcuts import get_object_or_404
from rest_framework import generics, permissions

from metsenat.models import Student, SponsoredAmounts
from metsenat.serializers import StudentSerializer, AddSponsorSerializer
from django_filters import rest_framework as filters


class StudentView(generics.ListCreateAPIView):
    permission_classes = [permissions.IsAuthenticated]
    queryset = Student.objects.order_by("-id")
    serializer_class = StudentSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_fields = ["student_type", "university"]


class StudentUpdateView(generics.RetrieveUpdateAPIView):
    permission_classes = [permissions.IsAuthenticated]
    queryset = Student.objects.order_by("-id")
    serializer_class = StudentSerializer
    lookup_url_kwarg = "student_id"


class StudentSponsorsListView(generics.ListAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = AddSponsorSerializer

    def get_queryset(self):
        student = get_object_or_404(Student, id=self.kwargs.get("student_id"))
        return SponsoredAmounts.objects.filter(student=student)
