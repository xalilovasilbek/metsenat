from rest_framework import generics, permissions
from rest_framework.permissions import SAFE_METHODS
from django_filters import rest_framework as filters

from metsenat.models import Sponsor
from metsenat.serializers import SponsorListCreateSerializer, SponsorUpdateSerializer


class SponsorCreateView(generics.ListCreateAPIView):
    queryset = Sponsor.objects.order_by("-id")
    serializer_class = SponsorListCreateSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_fields = ["status", "budget", "created_at"]

    def get_permissions(self):
        permission_class = self.permission_classes
        if self.request.method in SAFE_METHODS:
            permission_class = [permissions.IsAuthenticated]
        return [permission() for permission in permission_class]


class SponsorUpdateView(generics.RetrieveUpdateAPIView):
    permission_classes = [permissions.IsAuthenticated]
    queryset = Sponsor.objects.all()
    serializer_class = SponsorUpdateSerializer
