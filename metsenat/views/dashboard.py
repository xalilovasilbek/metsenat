from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from metsenat.utils import get_statistics


class DashboardView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        return Response(data=get_statistics())
