from django.shortcuts import get_object_or_404
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from metsenat.models import Student, SponsoredAmounts
from metsenat.serializers import AddSponsorSerializer, UpdateSponsoredAmountSerializer
from metsenat.services.validate_sponsored_amount import ValidateSponsoredAmount, ValidateExistingSponsoredAmount


class AddSponsoredAmount(APIView):
    permission_classes = [permissions.IsAuthenticated]

    @swagger_auto_schema(request_body=AddSponsorSerializer)
    def post(self, request, *args, **kwargs):
        student = self.get_object()

        serializer = AddSponsorSerializer(data=request.data)
        serializer.is_valid(True)

        amount = serializer.validated_data["amount"]
        sponsor = serializer.validated_data["sponsor"]
        ValidateSponsoredAmount(sponsor, student, amount).validate()

        sp_amount = SponsoredAmounts.objects.create(student=student, sponsor=sponsor, amount=amount)
        return Response(data=AddSponsorSerializer(instance=sp_amount).data)

    def get_object(self):
        student = get_object_or_404(Student, id=self.kwargs.get("student_id"))
        return student


class EditSponsoredAmount(APIView):
    permission_classes = [permissions.IsAuthenticated]

    @swagger_auto_schema(request_body=UpdateSponsoredAmountSerializer)
    def put(self, request, *args, **kwargs):
        sp_amount = self.get_object()
        student = sp_amount.student
        sponsor = sp_amount.sponsor

        serializer = UpdateSponsoredAmountSerializer(data=request.data)
        serializer.is_valid(True)

        amount = serializer.validated_data["amount"]
        ValidateExistingSponsoredAmount(sponsor, student, amount, sp_amount).validate()

        sp_amount.amount = amount
        sp_amount.save()
        return Response(data=AddSponsorSerializer(instance=sp_amount).data)

    def get_object(self):
        sp_amount = get_object_or_404(SponsoredAmounts, id=self.kwargs.get("sp_amount_id"))
        return sp_amount


class SponsoredAmountsRetrieveDeleteView(generics.RetrieveDestroyAPIView):
    permission_classes = [permissions.IsAuthenticated]
    queryset = SponsoredAmounts.objects.all()
    serializer_class = AddSponsorSerializer
    lookup_url_kwarg = "sp_amount_id"
