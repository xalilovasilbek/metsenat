from django.db import models
from django.db.models import Sum
from phonenumber_field.modelfields import PhoneNumberField

from metsenat.enums import StudentTypes
from metsenat.models.base import BaseModel


class Student(BaseModel):
    fullname = models.CharField(max_length=255)
    phone = PhoneNumberField()
    university = models.CharField(max_length=511)
    student_type = models.CharField(max_length=63, choices=StudentTypes.choices())
    contract_amount = models.PositiveBigIntegerField()

    def __str__(self):
        return self.fullname

    @property
    def allocated_amount(self):
        return self.sponsoredamounts_set.aggregate(received_amount=Sum("amount")).get("received_amount") or 0

    @property
    def space_amount(self):
        return self.contract_amount - self.allocated_amount
