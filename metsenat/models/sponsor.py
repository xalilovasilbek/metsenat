from django.db import models
from django.db.models import Sum
from phonenumber_field.modelfields import PhoneNumberField

from metsenat.enums import PersonalTypes, Statuses
from metsenat.models.base import BaseModel


class Sponsor(BaseModel):
    fullname = models.CharField(max_length=255)
    phone = PhoneNumberField()
    personal_type = models.CharField(max_length=63, choices=PersonalTypes.choices())
    organization = models.CharField(max_length=511, null=True)
    budget = models.PositiveBigIntegerField()
    status = models.CharField(max_length=63, choices=Statuses.choices(), default=Statuses.NEW.value)

    def __str__(self):
        return self.fullname

    @property
    def spent_amount(self):
        return self.sponsoredamounts_set.aggregate(spent_amount=Sum("amount")).get("spent_amount") or 0

    @property
    def balance(self):
        return self.budget - self.spent_amount


class SponsoredAmounts(BaseModel):
    sponsor = models.ForeignKey("metsenat.Sponsor", on_delete=models.CASCADE)
    student = models.ForeignKey("metsenat.Student", on_delete=models.CASCADE)
    amount = models.PositiveBigIntegerField()

    def __str__(self):
        return f"{self.sponsor} to {self.student} ({self.amount})"
